import static org.junit.Assert.*;

import org.junit.Test;

public class StringProcessorTest {
    @Test
    public void multiplyStringTest() {
        String str = "test";
        assertEquals(StringProcessor.multiplyString(str, 3), "testtesttest");
        assertEquals(StringProcessor.multiplyString(str, 0), "");
    }

    @Test (expected = IllegalArgumentException.class)
    public void multiplyStringTestWithException() {
        String str = "test";
        StringProcessor.multiplyString(str, -5);
    }

    @Test
    public void findQuantityInclusionTest() {
        String str1 = new String("Hello world");
        String str2 = new String("o");
        String str3 = new String("t");
        String str4 = new String("H");
        assertEquals(StringProcessor.findQuantityInclusion(str1, str2), 2);
        assertEquals(StringProcessor.findQuantityInclusion(str1, str3), 0);
        assertEquals(StringProcessor.findQuantityInclusion(str1, str4), 1);
    }

    @Test (expected = IllegalArgumentException.class)
    public void findQuantityInclusionTestWithException() {
        String str1 = new String("Hello world");
        StringProcessor.findQuantityInclusion(str1, "");
        StringProcessor.findQuantityInclusion(str1, null);
    }

    @Test
    public void turnIntoTextTest() {
        String str1 = new String("123Hello3221");
        String str2 = new String("Hello!");
        String str3 = new String("");
        assertEquals(StringProcessor.turnIntoText(str1), "ОдинДваТриHelloТриДваДваОдин");
        assertEquals(StringProcessor.turnIntoText(str2), "Hello!");
        assertEquals(StringProcessor.turnIntoText(str3), "");
    }

    @Test
    public void deleteAllSecondTest() {
        StringBuilder str1 = new StringBuilder("123456");
        StringBuilder str2 = new StringBuilder("H");
        StringBuilder str3 = new StringBuilder("");

        assertEquals(StringProcessor.deleteAllSecond(str1).toString(), "135");
        assertEquals(StringProcessor.deleteAllSecond(str2).toString(), "H");
        assertEquals(StringProcessor.deleteAllSecond(str3).toString(), "");
    }
}
