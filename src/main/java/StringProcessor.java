public class StringProcessor {

    //win!

    public static String multiplyString(String s, int N) {
     /*На входе строка s и целое число N. Выход — строка, состоящая из N копий строки s, записанных подряд. При N = 0
    результат — пустая строка. При N < 0 выбрасывается исключение*/
        String multiply = new String();

        if (N < 0) throw new IllegalArgumentException("Invalid number of copies");
        s = s.trim();
        for (int i = 0; i < N; i++) {
            multiply = multiply.concat(s);
        }
        return multiply;
    }

    //win!
    public static int findQuantityInclusion(String str1, String str2) {
     /*На входе две строки. Результат — количество вхождений второй строки в первую. Если вторая строка пустая или null,
    выбросить исключение*/
        if (str2.length() == 0 || str2 == null) throw new IllegalArgumentException("String is empty or equals null");

        int index = 0;
        int quantity = 0;

        while (index != -1) {
            if (quantity != 0) {
                index++;
            }
            index = str1.indexOf(str2, index);
            if (index>=0) {
                quantity++;
            }
        }
        return quantity;
    }

    //win!
    public static String turnIntoText(String str) {
    /*Постройте по строке новую строку, которая получена из исходной заменой каждого символа '1' на подстроку "один”,
    символа ‘2’ на подстроку “два” и символа ‘3’ на подстроку “три”*/
        str = str.replace("1", "Один");
        str = str.replace("2", "Два");
        str = str.replace("3", "Три");

        return str;
    }

    //win! А на что еще ее можно проверить?
    public static StringBuilder deleteAllSecond(StringBuilder str) {
     /*В строке типа StringBuilder удалите каждый второй по счету символ. Должна быть модифицирована входная строка, а
     не порождена новая*/
        for(int i = 1; i < str.length(); i = i + 1) {
            str.deleteCharAt(i);
        }
        return str;
    }
}